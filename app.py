#Importacion de paquetes necesarios para la ejecucion del codigo
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
import plotly.express as px
import numpy as np
import  Model
from Model import PollutantType

#se importa la clase flaskServer como servidor base
import flaskServer
from flaskServer import server

app = dash.Dash(server=server)

#Se lee el archivo Json con los datos con el paquete panada
json = pd.read_json('Datos_SIATA_Aire_pm25.json', convert_dates=True)

#se extraen los puntos de geo localizacion del Json
latitudes = json.latitud.values.tolist()
longitudes = json.longitud.values.tolist()

m = []
for k in range(0, 8760):#8760):
    fecha = json.datos[1][k].get('fecha')
    m=[]
    for i in range(21):
        dato = json.datos[i][k].get('valor')

        #Se valida que los dos cumplan con unos estandares minimos para se utilizables 
        if(Model.ValidateData(dato)):
            #Se realiza un tratamiento de datos para determinar indice de contaminacion en el punto
            m.append(Model.CalculateAQI(dato, PollutantType.PM25 ))
        else:
            latitudes[i] = None
            longitudes[i] = None

    m=np.array(m)

fig = go.Figure(go.Densitymapbox(lat=json.latitud.values.tolist(), lon=json.longitud.values.tolist(), z= m, radius = 100))

fig.update_layout(mapbox_style="stamen-terrain", mapbox_center_lon=-75.589900, mapbox_center_lat=6.240737, mapbox_zoom=10)
fig.update_layout(margin={'l': 0, 'r': 0, 'b': 0, 't': 0})

app.layout = html.Div([
        html.H1('Alerta AQI'),
        dcc.Graph(id='map', figure=fig)
    ])


if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0', port=80)
