#importando la libreria
import numpy as np
grid_x, grid_y = np.mgrid[0:1:100j, 0:1:200j]
def CreateGrid(points, values):
    from scipy.interpolate import griddata
    return griddata(points, values, (grid_x, grid_y), method='linear')
    #grafico los resultados de los tres ejemplos de inteprolacion
   
