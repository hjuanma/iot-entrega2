#Importacion de paquetes necesarios para la ejecucion del codigo
from flask import Flask, render_template, request, url_for, redirect, Response, jsonify
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime, timedelta  

server = Flask(__name__)

#Se ingresa la referencia a la db que se usara, ya sea sqlite o Mysql
server.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database/iot.db'

db = SQLAlchemy(server)

###### MODELOS DE DATOS PARA LA DB Y MANEJO DE OBJETOS EN EL CODIGO ######
###### Estos modelos tambien los usara de referencia el FrameWork flask para crear las tablas y relaciones de la db ######
#Se crea un modelo para los objetos sensores
class  Sensor(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    active = db.Column(db.Boolean)
    
#Se crea un modelo para los objetos 'Records' los que tendran el registro de la los datos enviado por los sensores
class Record(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sensorId = db.Column(db.Integer)
    temperature = db.Column(db.String(200))
    humidity = db.Column(db.Float)
    longitude = db.Column(db.Float)
    latitude = db.Column(db.Float)
    date = db.Column(db.DateTime)


##### RECORDS #####
# se consultan todos los registros que se tienen hasta el momento y se envian a la ruta /records para ser pitados en la interfaz
@server.route('/records')
def records():
    records = Record.query.all()
    return render_template('records.html', records=records)

#se consulta un registro espesifico
@server.route('/records/<id>', methods=['GET'])
def getRecord(id):
    record = Record.query.all().filter(Record.id == id)
    response = jsonify({
            'id': str(id),
            'sensor' : record.sensorId,
            'latitud': record.latitude,
            'longitud': record.longitude,
            'temperatura': record.temperature,
            'humedad' : record.humidity,
            'fecha' : record.date
        })
    if len(response) > 0:
        return Response(response, mimetype='application/json')
    else:
        return {'message': 'Error'}

# metodo que recibe los registros y los almacena en la DB
@server.route('/records', methods=['POST'])
def creteRecord():
    # Recivir datos
    sensor = request.json['sensor']
    latitud = request.json['latitud']
    longitud = request.json['longitud']
    temperatura = request.json['temperatura']
    humedad = request.json['humedad']
    fecha = datetime.now() 

    if latitud and temperatura and longitud and humedad:
        
        record = Record(sensorId = sensor, latitude = latitud, longitude = longitud, temperature =  temperatura, humidity = humedad, date =  fecha)
        
        db.session.add(record)
        db.session.commit()

        response = jsonify({
            '_id': str(id),
            'sensor' : sensor,
            'latitud': latitud,
            'longitud': longitud,
            'temperatura': temperatura,
            'fecha' : fecha
        })
        response.status_code = 201
        return response
    else:
        return {'message': 'Error'}

##### SENSORES #####
@server.route('/create_sensor', methods=['POST'])
def createSensor():
    sensor = Record(content=request.form['content'], done=False)
    if len(sensor.content) > 0:
        db.session.add(sensor)
        db.session.commit()
    return redirect(url_for('records'))

@server.route('/activate/<int:id>')
def done(id):
    sensor = Sensor.query.filter_by(id=id).first()
    sensor.active = not sensor.active
    db.session.commit()
    return redirect(url_for('records'))


if __name__ == '__main__':
    server.run(debug=True, host='0.0.0.0', port=80)
