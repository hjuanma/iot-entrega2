#include <Wire.h>
#include <LiquidCrystal.h> 
#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>


const char* ssid = "Yepesvillega2";
const char* password = "3ZLY740L";

//Your Domain name with URL path or IP address with path
const char* serverName = "http://54.173.141.167:80/records";

unsigned long lastTime = 0;

// Set timer to 5 seconds (5000)
unsigned long timerDelay = 5000;


//Configuracion de pines de lectura para el GPS
static const int RXPin = 2, TXPin = 0;
static const uint32_t GPSBaud = 9600;

//Definicion de estados
enum State_enum {GPS, TEMP, HUM, END};

uint8_t state;

double temperature = 0;
double humidity = 0;
String latitud = "";
String longitud = "";

int id = 1001;

//Numero de muesteas que se promediaran en la temperatura y la humedad
int tempRep = 20;
int humidityRep = 5;

String GPSResponse = "";

//Direccion del HDC1080
#define Addr 0x40

// Objeto TinyGPS++
TinyGPSPlus gps;

//Coneccion del dispositivo GPS
SoftwareSerial ss(RXPin, TXPin);

void setup() 
{
  Wire.begin();
  
  Serial.begin(9600);
  ss.begin(GPSBaud);


  WiFi.begin(ssid, password);
  Serial.println("Conectando");
  while(WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Conectado con la ip: ");
  Serial.println(WiFi.localIP());
 
  
  //Configuracion HDC1080
  Wire.beginTransmission(0x40);
  Wire.write(0x02);
  Wire.write(0x90);
  Wire.write(0x00);
  Wire.endTransmission();
 
  //Configuracion del estado inicial
  state = GPS;

  //Delay para el inicio del HDC1080
  delay(20);

}

void loop() {
  stateMachine();
}


void stateMachine()
{
  
  switch(state)
  {    
    case GPS:
      GPSFun();
      state = TEMP;
    break;
      
    case TEMP:
      TemFun();
      state = HUM;
    break;
    
    case HUM:
      HumFun();
      state = END;
    break;
    
    case END:
      //Send an HTTP POST request every 10 minutes
      if ((millis() - lastTime) > timerDelay) {
        EndFun();
        state = TEMP;
      }
    break;

  }
  
}

void TemFun()
{
  
  //Inicialisacion de variables
  double tempProm = 0;
  
  //Toma de datos por recusividad
  tempProm = ReadTemp(tempRep);

  //Calculo de promedio y regresion linear
  temperature = PromedioLineal(tempProm,tempRep, 1, 0 );  
}

void HumFun()
{
  
  //Inicialisacion de variables
  double humidityProm = 0; 

  //Toma de datos por recusividad
  humidityProm = ReadHumidit(humidityRep);

  //Calculo de promedio y regresion linear
  humidity = PromedioLineal(humidityProm,humidityRep, 1, 0 );
}



void GPSFun()
{
  //valida qur la informacion esta armada de forma correcta

  bool reading = true;

  while(reading)
  {
    while(ss.available() > 0)
    {
      if (gps.encode(ss.read()))
      {
        reading = false;
        
          if (gps.location.isValid()){ // Verifica si la lectura del GPS es valida
            latitud = String(gps.location.lat(),6);
            longitud = String(gps.location.lng(),6);
          }
          else{// Si la lectura no es valida la latitud y longitud seran 0
            latitud = String(0.0,6);
            longitud = String(0.0,6);
          }
      }
    }    
  }
}

void EndFun(){
  

  sendthingspeak();
  
  delay(5000);

}

void sendthingspeak()
{
    //Check WiFi connection status
    if(WiFi.status()== WL_CONNECTED)
    {
      
      String PostData = "";
      HTTPClient http;
      
      // Your Domain name with URL path or IP address with path
      http.begin(serverName);

      PostData = String("{\"sensor\":"+String(id)+", \"temperatura\":"+String(temperature,7)+", \"humedad\":"+String(humidity,7)+", \"longitud\":"+longitud+", \"latitud\":"+latitud+"}");
      
      // If you need an HTTP request with a content type: application/json, use the following:
      http.addHeader("Content-Type", "application/json");
      int httpResponseCode = http.POST(PostData);
     
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
      
      Serial.println(PostData);
        
      // Free resources
      http.end();
    }
    else {
      Serial.println("WiFi Disconnected");
    }
    lastTime = millis();
}


double PromedioLineal(double data, int iterations, double slope, double intersection)
{

  data = data/iterations;
  
  return(slope)*data - intersection;
}


double ReadHumidit(int iterations)
{

  uint8_t Byte[4]; 

  uint16_t humidity;

  uint16_t result;
  
  Wire.beginTransmission(Addr);
  Wire.endTransmission();
  
  //tempo para asegurar la conversion
  delay(20);
  
  Wire.requestFrom(Addr, 4);

  delay(1);
  
  //valida que se tomaron los datos
  if (4 <= Wire.available())
  {
    
    //bytes de la lectura de humedad
    Byte[3] = Wire.read();
    Byte[4] = Wire.read(); 

   //Combina los dos bytes para hacer un int de 16 bits.
    humidity = (((unsigned int)Byte[3] <<8 | Byte[4]));

    //Humidity(%) = reading/(2^16)*100%
    result = (double)(humidity)/(65536)*100;

    if(iterations == 1)
    {
      return result;
    }
    else
    {
      //Llamado recursivo y espera de 1 segundo
      delay(1000);
      return(result + ReadHumidit(iterations-1));
    }
  }
}


double ReadTemp(int iterations)
{
  unsigned int data[2];

  Wire.beginTransmission(Addr);
  Wire.write((byte)0x00);

  Wire.endTransmission();
  delay(500);
   
  Wire.requestFrom(Addr, 2);

  //Captura de datos del sensor 
  if (Wire.available() == 2)
  {
    data[0] = Wire.read();
    data[1] = Wire.read();
  }

  int temp = (data[0] * 256) + data[1];
  float cTemp = (temp / 65536.0) * 165.0 - 40;
      
    if(iterations == 1)
    {
    return cTemp;
    }
    else
    {
      //Llamado recursivo
      return(cTemp + ReadTemp(iterations-1));
    }
}
